import matplotlib.pyplot as plt
from util import *

model = get_augmented_ich360_model()
with model.copy() as wt_model:
    wt_model.reactions.get_by_id("EX_glx_e").bounds = (-1, 0)
    wt_model.reactions.get_by_id("glx_t").bounds = (-1000, 1000)
    res = wt_model.optimize()
    wt_growth_rate_on_glx = res.objective_value


def plot_gbr(minimal: bool = True):
    strain_df = get_designed_strains_iCH360(minimal=minimal)
    data = []
    for row in strain_df.itertuples():
        try:
            with model as m:
                for ko in row.knock_outs:
                    m.reactions.get_by_id(ko).knock_out()
                for cs in row.carbon_sources:
                    m.reactions.get_by_id(f"EX_{cs}_e").lower_bound = -OTHER_CARBON_SOURCE_UPTAKE
                    if cs == "for":
                        add_c1_module(m)
                m.reactions.get_by_id(TARGET_REACTION).bounds = (0, 0)
                solution_wo_target = m.optimize()
                assert solution_wo_target.objective_value < MIN_GROWTH_RATE, f"KO strain {row.name} is not glyoxylate dependent"
                m.reactions.get_by_id(TARGET_REACTION).bounds = (0, TARGET_REACTION_FLUX)
                solution_with_target = m.optimize()
                assert solution_with_target.objective_value > MIN_GROWTH_RATE, f"KO strain {row.name} cannot grow even with glyoxylate"
                data.append((row.name, TARGET_REACTION_FLUX/solution_with_target.objective_value))
        except AssertionError:
            continue
    strain_growth_df = pd.DataFrame(data=data, columns=["strain", "GBR"])
    strain_growth_df["normed_GBR"] = strain_growth_df.GBR * wt_growth_rate_on_glx
    result_df = strain_df.join(strain_growth_df.set_index("strain"), on="name")
    fig, ax = plt.subplots(1, 1, figsize=(5, 4))
    bars = ax.bar(strain_growth_df.strain, strain_growth_df.normed_GBR.round(2))
    ax.set_xticks(range(strain_growth_df.shape[0]), labels=strain_growth_df.strain, rotation=45)
    ax.bar_label(bars)
    ax.set_ylabel("Dependence on glyoxylate")
    ax.set_xlabel("strain")
    fig.tight_layout()
    return fig, result_df

