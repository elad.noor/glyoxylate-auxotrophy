import cobra
import pandas as pd
import pooch
import zipfile

TARGET_REACTION = "glx_t"
TARGET_REACTION_FLUX = 1.0 # mmol/gCDW/h
EPSILON = 1e-4
OTHER_CARBON_SOURCE_UPTAKE = 1000.0 # mmol/gCDW/h
MIN_GROWTH_RATE = 0.01 # 1/h
ICH360_POOCH_ARGS = {
    "url": "doi:10.5281/zenodo.11092782/iCH360.zip",
    "known_hash": "md5:c3fef18931e4d527636d4e34dcc0abff",
}


def add_reaction(
        model, id: str, name: str, metabolites_to_add: dict, lower_bound: float = 0.0, upper_bound: float = 1000.0
) -> cobra.Reaction:
    rxn = cobra.Reaction(id=id, name=name, lower_bound=lower_bound, upper_bound=upper_bound)
    model.add_reactions([rxn])
    rxn.add_metabolites(metabolites_to_add)
    return rxn


def add_metabolite(
        model, id: str, formula: str, name: str
) -> cobra.Metabolite:
    met = cobra.Metabolite(id=id, formula=formula, name=name)
    model.add_metabolites([met])
    return met


def get_augmented_ich360_model() -> cobra.Model:
    # Load the standard iCH360 model (the link to GitHub is not permanent, this needs to be solved
    # e.g. by uploading the model to Zenodo or issuing a release on Git).
    fname = pooch.retrieve(**ICH360_POOCH_ARGS)
    with zipfile.ZipFile(fname, "r") as zip_ref:
        sbml = zip_ref.read("ich360_zenodo/Model/iCH360/Escherichia_coli_iCH360.xml")
        model = cobra.io.read_sbml_model(sbml.decode("utf-8"))

    # set the bounds for glucose uptake to 0
    model.reactions.get_by_id("EX_glc__D_e").bounds = (0, 0)

    # we remove the maintenance energy requirement (ATPM) in order to get a precise
    # estimate of the growth rates at low uptake rates (there is a mathematical reason,
    # we need to look at small values to calculate the derivative).
    model.reactions.ATPM.bounds = (0, 0)

    model.reactions.PFL.bounds = (0, 0)

    # The following metabolites and reactions are missing in iCH360 and added here:
    add_metabolite(model, id="2h3oppan_c", formula="C3H3O4", name="2-Hydroxy-3-oxopropanoate")  # AKA Tartronate semialdehyde
    add_metabolite(model, id="glx_e", formula="C2H1O3", name="glyoxylate")
    add_reaction(model, id="EX_glx_e", name="glyoxylate exchange", upper_bound=0, metabolites_to_add={"glx_e": -1})
    add_reaction(model, id="glx_t", name="glyoxylate transporter", metabolites_to_add={"glx_e": -1, "h_e": -1, "glx_c": 1, "h_c": 1})
    add_reaction(model, id="BHC", name="aspartate-glyoxylate transaminase", metabolites_to_add={"asp__L_c": -1, "glx_c": -1, "oaa_c": 1, "gly_c": 1})
    add_reaction(model, id="GLXCL", name="glyoxalate carboligase", metabolites_to_add={"glx_c": -2, "h_c": -1, "2h3oppan_c": 1, "co2_c": 1})
    add_reaction(model, id="TRSARr", name="tartronate semialdehyde reductase", metabolites_to_add={"2h3oppan_c": -1, "h_c": -1, "nadh_c": -1, "glyc__R_c": 1, "nad_c": 1})
    return model

def add_c1_module(model: cobra.Model) -> None:
    # only add this reaction for the C1+GLYAUX strain
    add_reaction(
        model, id="FTHFLi", name="Formate-tetrahydrofolate ligase",
        metabolites_to_add={"atp_c": -1, "for_c": -1, "thf_c": -1, "10fthf_c": 1, "adp_c": 1, "pi_c": 1}
    )

def get_designed_strains_iCH360(minimal: bool = False) -> pd.DataFrame:
    if minimal:
        return pd.DataFrame(
            columns=["name", "knock_outs", "carbon_sources"],
            data=[
                ["2OXOAUX", ("CS", ), ("succ", "glx")],  # GLXCL, MALS, and AKGDH are not required
                ["C1+GLYAUX", ("ICL", "GHMT2r", ), ("glyc", "glx")], # MALS is redundant when ICL is knocked out, GLXCL is not required
                ["TCAAUX", ("ICL", "PPC", ), ("glyc", "glx")],  # GLXCL is not required, malic enzymes are irreversible in the model
                ["UPPAUX", ("ENO", "ICL", ), ("succ", "glx")],  # MALS is redundant when ICL is knocked out
                ["LOWAUX", ("TPI", ), ("glyc", "glx")],  # ICL and MALS are not required
            ]
        )
    else:
        return pd.DataFrame(
            columns=["name", "knock_outs", "carbon_sources"],
            data=[
                ["2OXOAUX", ("GLXCL", "MALS", "CS", "AKGDH"), ("succ", "glx")],
                ["C1+GLYAUX", ("GLXCL", "MALS", "ICL", "GHMT2r"), ("glyc", "glx")],
                ["GLYAUX", ("GLXCL", "MALS", "ICL", "GHMT2r"), ("glyc", "for", "glx")],
                ["TCAAUX", ("GLXCL", "ICL", "PPC", "ME1", "ME2"), ("glyc", "glx")],
                ["UPPAUX", ("MALS", "ICL", "ENO"), ("succ", "glx")],
                ["LOWAUX", ("TPI", ), ("glyc", "glx")],
                #["2OXO+TCAAUX", ("ICL", "GLXCL", "SUCDi", "PPC"), ("glyc", "akg", "glx")],
            ]
        )