# Glyoxylate Auxotrophy


## Methods

### The stoichiometric model

In order to identify promising knockout strategies in *E. coli*
that create glyoxylate auxotrophy at different dependency levels,
we used the recently published
[*i*CH360 model](https://github.com/marco-corrao/iCH360).
*i*CH360 is a subnetwork of the much larger genome-scale model,
focusing on central metabolic subsystems that carry relatively
high flux, are central to maintaining and reproducing the cell,
and provide precursors and energy to engineered metabolic pathways.
This medium-sized model, by doing away with low-flux and secondary
pathways and enzymes, facilitates applications like ours since
these redundant reactions typically create bypasses that are
unlikely to be relevant in vivo and greatly complicate the search.
However, since we wanted to design auxotrophic strains to a
non-standard carbon source (glyoxylate), we had to augment the
*i*CH360 model with 2 metabolites and 5-6 reactions in order for
it to be able to deal with glyoxylate metabolism:

#### Metabolites:
- extracellular glyoxylate (`glx_e`): formula = C3H3O4
- 2-Hydroxy-3-oxopropanoate (`2h3oppan_c`): formula = C2H1O3
Note that 2-Hydroxy-3-oxopropanoate and tartronate semialdehyde are synonyms.

#### Reactions:
- glyoxylate exchange (EX_glx_e): `glx_e` ⇔ 
- glyoxylate transport (glx_t): `glx_e` + `h_c` ⇔ `glx_c` + `h_c`
- aspartate-glyoxylate transaminase (BHC): `asp__L_c` + `glx_c` ⇔ `oaa_c` + `gly_c`
- glyoxylate carboligase (GLXCL): `glx_c` + `h_c` ⇔ `2h3oppan_c` + `co2_c`
- tartronate semialdehyde reductase (TRSARr): `2h3oppan_c` + `h_c` + `nadh_c` ⇔ `glyc__R_c` + `nad_c`
- formate-tetrahydrofolate ligase (FTHFLi*): `for_c` + `atp_c` + `thf_c` ⇔ `10fthf_c` + `adp_c` + `pi_c`

\* The FTHFLi reaction was added only in the strain named C1+GLYAUX which utilizes formate as the source for all the C1 carbon metabolism.


### Searching for auxotrophic knockouts
After establishing a suitable stoichiometric model as described
in the previous section, we applied a search algorithm for
identifying auxotrophic knockout strategies.
First, the ATP maintenance reaction was removed, as the algorithm
is designed to only calculate the marginal dependence on
glyoxylate (at low growth rates) and the maintenance reaction is
not relevant for that calculation. Then, we set the bounds of the
glucose exchange flux to 0 (instead of the default lower bound
of -10 mmol/gCDW/h), and replaced it with succinate or glycerol
as the abundant carbon source, by setting the lower bound to
-1000 mmol/gCDW/h.
The next step is to iterate through all possible single or double
knockouts of reactions from the list of central reactions - i.e.
reactions that exist in the core model of *E. coli* and
the ones added to *i*CH360 (excluding the glyoxylate uptake itself).
In some cases, we lumped together two reactions
(denoted as REACTION1|REACTION2), either because they are arranged
in one linear pathway with no branchpoints, catalyzed by the same
enzyme, or the same chemical reaction with different cofactors
(catalyzed by two isoenzymes):
- G6PDH2r
- PGL
- PGI
- PFK
- FBP
- FBA
- TPI
- PGK
- GAPD
- PGM
- ENO
- PYK
- PPS
- PDH
- PFL
- GND
- RPE
- RPI
- TKT1|TKT2
- TALA
- PPC
- PPCK
- ME1|ME2
- SUCDi
- FUM
- CS
- ACONTa|ACONTb
- ICDHyr
- ICL
- MALS
- AKGDH
- SUCOAS
- MDH
- ALCD2x
- ACALD
- GLXCL|TRSARr
- GHMT2r
- BHC

For each possible single or double knockout, we run a series
of flux-balance analyses (FBA) using the
[cobrapy toolbox](https://opencobra.github.io/cobrapy/).
We first test whether it can grow at all on succinate.
If it can, we run another FBA with a high abundance of succinate,
and a limiting amount of glyoxylate. The ratio between the
maximal growth rate and the glyoxylate uptake rate represents
that GBR. The same is repeated again with glycerol instead
of succinate.
We then normalized the GBR value by dividing them by the GBR
of a cell growing on glyoxylate alone (without succinate nor
glycerol). This brings the values to generally be between 0
and 1, except for a few cases where the KO makes the cell
require even more glyoxylate.

## Usage

First, to set up the python virtual environment,
open a command line prompt and run the following commands:

```bash
git clone https://gitlab.com/elad.noor/glyoxylate-auxotrophy.git
cd glyoxylate-auxotrophy
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt 
```

### Regenerating data and plots from [Orsi et al. 2024](https://doi.org/10.1101/2024.08.23.609350 )
Start Jupyter with: `jupyter lab` and open the notebook called `plot_figure.ipynb`

### Table of growth-coupled designs for up to 3 KOs
Run the following command:
```
python find_glyoxylate_auxotrophcs.py
```
Note that this script has a long runtime, and requires
many computing resources. You can also skip running it and
see the final result directly at:
`res/auxotrophic_knockouts_max_3_KOs.csv`