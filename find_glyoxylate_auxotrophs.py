import numpy as np
import seaborn as sns
from optslope import calculate_slope_multi
from tqdm import tqdm
import matplotlib.pyplot as plt
from typing import Tuple, List, Optional
from util import *


model = get_augmented_ich360_model()
strain_df = get_designed_strains_iCH360(minimal=True)


with model.copy() as wt_model:
    wt_model.reactions.get_by_id("EX_glx_e").bounds = (-1, 0)
    wt_model.reactions.get_by_id("glx_t").bounds = (-1000, 1000)
    res = wt_model.optimize()
    wt_growth_rate_on_glx = res.objective_value


DEFAULT_NUM_CPUS = 1  # the number of CPUs to use in parallel (set to 1 if unsure)
DEFAULT_MAX_NUM_KO = 2  # the maximal number of knockouts to consider in each design
carbon_source_list = [("succ", "glx"), ("glyc", "glx")]
single_ko_list = [
    'G6PDH2r',
    'PGL',
    'PGI',
    'PFK',
    'FBP',
    'FBA',
    'TPI',
    'PGK',
    'GAPD',
    'PGM',
    'ENO',
    'PYK',
    'PPS',
    'PDH',
    'PFL',
    'GND',
    'RPE',
    'RPI',
    'TKT1|TKT2',
    'TALA',
    'PPC',
    'PPCK',
    'ME1|ME2',
    'SUCDi',
    'FUM',
    'CS',
    'ACONTa|ACONTb',
    'ICDHyr',
    'ICL',
    'MALS',
    'AKGDH',
    'SUCOAS',
    'MDH',
    'ALCD2x',
    'ACALD',
    'GLXCL|TRSARr',
    'GHMT2r',
    'BHC',
]


def calculate_slopes(max_num_ko: int = DEFAULT_MAX_NUM_KO, num_processes: int = DEFAULT_NUM_CPUS) -> pd.DataFrame:
    dfs = []
    print(
        f"Calculating slopes for up to {max_num_ko} knockouts, and "
        f"for {len(carbon_source_list)} carbon source combinations"
    )

    for carbon_sources in tqdm(
        carbon_source_list,
        total=len(carbon_source_list),
        desc="Carbon Sources",
    ):
        df = calculate_slope_multi(
            wt_model=model,
            carbon_sources=carbon_sources,
            single_knockouts=single_ko_list,
            target_reaction=TARGET_REACTION,
            max_knockouts=max_num_ko,
            num_processes=num_processes,
            chunksize=10,
        )
        dfs.append(df)
    return pd.concat(dfs)


def ko_to_indices(knockouts: List[str]) -> Optional[Tuple[int, int]]:
    if len(knockouts) == 0:
        return None, None

    if len(knockouts) == 1:
        i = single_ko_list.index(knockouts[0])
        return i, i
    else:
        return tuple(sorted(map(single_ko_list.index, knockouts)))


def plot_glyoxylate_auxotrophs(result_df, strain_df, carbon_source_list) -> plt.Figure:
    fig, axs = plt.subplots(2, 1, figsize=(12, 20))

    for ax, cs in zip(axs.flat, carbon_source_list):
        ax.set_title(cs[0])
        N = len(single_ko_list)
        data_mat = np.zeros((N, N)) - 1
        for row in result_df[result_df.carbon_sources == cs].itertuples():
            i0, i1 = ko_to_indices(row.knockouts)
            if i0 is not None:
                # normalize the GBR to be relative to the dependence of wild-type E. coli growing only on glyoxylate
                # note, that the slope can still be larger than 1, for example in knockouts where glyoxylate utilization is impaired (but not completely)
                if ~np.isnan(row.slope):
                    data_mat[i0, i1] = row.slope * wt_growth_rate_on_glx

        # make a colormap which assigns a red color to values around 0,
        # and uses Viridis for all positive values
        cmap = [(0.3, 0.3, 0.3)] + sns.color_palette("Blues_r", 100)

        g = sns.heatmap(data_mat.T, vmin=0, vmax=0.5, cmap=cmap, annot=False, linewidth=.2, mask=(data_mat.T < 0),
                        cbar_kws={'label': 'Dependence on glyoxylate'}, ax=ax)
        g.set_facecolor('#C0C0C0')
        for i in range(N):
            for j in range(N):
                if data_mat[j, i] < 0:
                    continue
                elif data_mat[j, i] == 0:
                    annotation = "0"
                elif data_mat[j, i] < 0.5:
                    annotation = f"{data_mat[j, i]:.2f}"
                    annotation = annotation[1:]
                else:
                    annotation = ">.5"
                ax.text(j+0.5, i+0.5, annotation, ha="center", va="center", color="white" if data_mat[j, i] < 0.25 else "black", fontsize=8)

        ax.set_xticks(np.arange(0, N) + 0.5)
        ax.set_yticks(np.arange(0, N) + 0.5)
        ax.set_xticklabels(single_ko_list, rotation=90, ha='center', fontsize=12)
        ax.set_yticklabels(single_ko_list, rotation=0, va='center', fontsize=12)
        for row in strain_df.itertuples():
            if set(cs) == set(row.carbon_sources):
                if len(row.knock_outs) <= 2:
                    i0, i1 = ko_to_indices(row.knock_outs)
                    ko_text = " ".join(["Δ" + k for k in row.knock_outs])
                    ax.annotate(
                        text=row.name + f" ({ko_text})", xy=(i0+0.5, i1), xytext=(i0+2, i0-5),
                        arrowprops={"width": 0.5, "color": "black"}, color="black",
                        fontsize=14, bbox={"boxstyle": "round", "fc": "0.8"}
                    )
                    ax.plot([i0, i0+1, i0+1, i0, i0], [i1, i1, i1+1, i1+1, i1], 'k-', linewidth=2)

    return fig

