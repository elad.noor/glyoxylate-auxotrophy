# %%
# -*- coding: utf-8 -*-
import sys, os
import numpy as np
import pandas as pd
import cobra

print('Python version:', sys.version)
print('numpy version:', np.__version__)
print('pandas version:', pd.__version__)
print('cobrapy version:', cobra.__version__)

# %%
def KORxn(model: cobra.Model,
          rxns2KO: list):
    """Function for knocking out reactions."""
    for ID in rxns2KO:
        reaction = model.reactions.get_by_id(ID)
        reaction.knock_out()

# %%
def pfba(model: cobra.Model):
    cobra.flux_analysis.pfba(model)
    
# %%
def flux2file(model: cobra.Model, 
              psw, product, output_dir='tmp'):
    """Function of exporting flux data."""
    n = len(model.reactions)
    modelMatrix = np.empty([n, 9], dtype = object)
    for i in range(len(model.reactions)):
        x = model.reactions[i]
        modelMatrix[i, 0] = i + 1
        modelMatrix[i, 1] = x.id
        modelMatrix[i, 2] = x.name
        modelMatrix[i, 3] = x.reaction
        modelMatrix[i, 4] = x.subsystem
        modelMatrix[i, 5] = x.lower_bound
        modelMatrix[i, 6] = x.upper_bound
        modelMatrix[i, 7] = x.flux
        modelMatrix[i, 8] = abs(x.flux)
        
    df = pd.DataFrame(data = modelMatrix, 
                      columns = ['N', 'RxnID', 'RxnName', 'Reaction', 'SubSystem', 
                      'LowerBound', 'UpperBound', 'Flux-core', 'abs(Flux)'])
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    filepath = os.path.join(output_dir, '{}_{}.xlsx'.format(psw, product))
    df.to_excel(filepath, index=False)

# %% [markdown]
# ## Model background
# 
#  * Using _E. coli_ full model *i*ML1515.
#  * Modified the transhydrogenase reaction (THD2pp) from 2 protons translocation to 1.
#  * Changed HSDy (homoserine DH) to be irrversible, towards to homS.
#  * Changed ICL (isocitrate lyase) to be reversible. 
#  * Changed TRPAS2 (Tryptophanase) to be irreversible, towards tryptophane degradation. 
#  * Base deletions: PFL, OBTFL, POR5 (pyruvate synthase, Ferredoxin), FDH4pp, FDH5pp, GLYCK (glycerate kinase, to 3pg), FRD2 and FRD3.
#  * FolD is reversible, MTHFC and MTHFD, equivalent to Fch and MtdA.
#  * FTL reaction is existed, FTHFLi (irrversible)
#  * GlyA (GHMT2r) is reversible.

# %%
# Load full model and modification 
model = cobra.io.load_json_model('iML1515.json')

model.reactions.THD2pp.add_metabolites({"h_p": 1, "h_c": -1})
model.reactions.HSDy.bounds = (-1000, 0)
model.reactions.ICL.bounds = (-1000, 1000)
model.reactions.TRPAS2.bounds = (0, 1000)

wt = model.copy()

KORxn_base = ['POR5', 'GLYCK', 'FDH4pp', 'FDH5pp',
              'PFL', 'OBTFL', 'GART', 'DRPA', 'PAI2T',
              'FRD2','FRD3','CITL',
              'GLYCLTDy','GLYCTO2','GLYCTO3','GLYCTO4','UGLYCH','DHGLYH',
              'HPYRRx','HPYRRy',
              'ATPM']

KORxn(model, KORxn_base)
# %%
model.reactions.EX_glc__D_e.bounds = (0,0)

rxn1=cobra.Reaction(id='EX_glx',name='glyoxylate exchange',lower_bound=0,upper_bound=0)
rxn2=cobra.Reaction(id='BHC',name='aspartate-glyoxylate transaminase',lower_bound=0,upper_bound=1000)

model.add_reactions([rxn1, rxn2])

rxn1.add_metabolites({'glx_c':-1,'h_c':-1})
rxn2.add_metabolites({'asp__L_c':-1,'glx_c':-1,'oaa_c':1,'gly_c':1})
# %% [markdown]
# - Set unlimited main carbon source (succinate: `succ`, or glycerol: `glyc`), and limited glyoxylate (`glx`, 1 mmol/gCDW/h)   
#
#| abbreviation | genotype                                  | carbon source  |
#|--------------|-------------------------------------------|----------------|
#| WT           | `WT*`                                     | `glx`          |
#| 2OXOAUX      | `gcl aceB-glcB gltA-prpC sucAB`           | `succ+glx`     |
#| C1+GLYAUX    | `gcl aceB-glcB aceA ltaE kbl-tdh glyA`    | `glyc+glx`     |
#| TCAAUX       | `gcl aceA maeAB ppc`                      | `glyc+glx`     | 
#| UPPAUX       | `aceB-glcB aceA eno`                      | `succ+glx`     |
#| LOWAUX       | `tpi`                                     | `glyc+glx`     |
#| GLYAUX       | `gcl aceB-glcB aceA ltaE kbl-tdh glyA`    | `glyc+for+glx` |
#
# List of mappings from genes to reactions:
#
# | gene        | BiGG reaction | enzyme name                                  | EC number | Present in iCH360? |
# |-------------|---------------|----------------------------------------------|-----------|--------------------|
# | `aceB-glcB` | `MALS`        | Malate syntase                               | 2.3.3.9   | yes                |
# | `gltA-prpC` | `CS`          | Citrate synthase                             | 2.3.3.1   | yes                |
# | `sucAB`     | `AKGDH`       | 2-Oxogluterate dehydrogenase                 | 1.2.4.2   | yes                |
# | `gcl`       | `GLXCL`       | Glyoxalate carboligase                       | 4.1.1.47  | no                 |
# | `aceA`      | `ICL`         | Isocitrate lyase                             | 4.1.3.1   | yes                |
# | `maeAB`     | `ME1 & ME2`   | Malic enzyme NAD(P)                          | 1.1.1.39  | yes                |
# | `ppc`       | `PPC`         | Phosphoenolpyruvate carboxylase              | 4.1.1.31  | yes                |
# | `eno`       | `ENO`         | Enolase                                      | 4.2.1.11  | yes                |
# | `tpiA`      | `TPI`         | Triose phosphate isomerase                   | 5.3.1.1   | yes                |
# | `pck`       | `PPCK`        | Phosphoenolpyruvate carboxykinase            | 4.1.1.49  | yes                |
# | `frd`       | `FRD2 & FRD3` | Fumarate reductase                           | 1.3.99.1  | yes                |
# | `sdh`       | `SUCDi`       | Succinate dehydrogenase (irreversible)       | 1.3.5.1   | yes                |
# | `glyA`      | `GHMT2r`      | Glycine hydroxymethyltransferase, reversible | 2.1.2.1   | Yes                |
# | `kbl-tdh`   | `GLYAT`       | Glycine C-acetyltransferase                  | 2.3.1.29  | no                 |
# | `ltaE`      | `THRA & THRA2`| low-specificity L-threonine aldolase         | 4.1.2.48  | no                 |
# | `mgsA`      | `MGSA`        | methylglyoxal synthase                       | 4.2.3.3   | no                 |
# | `edd`       | `EDD`         | 6-phosphogluconate dehydratase               | 4.2.1.12  | no                 |

# %%
df = pd.DataFrame(
    columns=['name','knock_outs','carbon_sources'],
    data=[
        ['2OXOAUX', ['GLXCL','MALS','CS','AKGDH'], ('succ','glx')],
        ['C1+GLYAUX', ['GLXCL','MALS','ICL','GHMT2r','THRA','THRA2','GLYAT'], ('glyc','glx')],
        ['TCAAUX', ['GLXCL','ICL','PPC','ME1','ME2'], ('glyc','glx')],
        ['UPPAUX', ['MALS','ICL','ENO','THRA','THRA2','GLYAT'], ('succ','glx')],
        ['LOWAUX', ['TPI','MGSA','EDD'], ('glyc','glx')],
    ]
)

# %%
for i in df.index: 
    with model as m:
        KORxn(m,df.loc[i,'knock_outs'])
        m.reactions.get_by_id(f"EX_{df.loc[i,'carbon_sources'][0]}_e").lower_bound=-1000
        m.reactions.EX_glx.lower_bound = -1
        pfba(m)
        df.loc[i,'Growth_rate'] = m.reactions.BIOMASS_Ec_iML1515_core_75p37M.flux
        # print(m.summary())
        # flux2file(m,'glx',i)

# %%
# for WT
with model as m: 
    m.reactions.EX_glx.lower_bound = -1
    pfba(m)
    i=len(df)
    df.loc[i]=['WT',None,'glx','NaN']
    df.loc[i,'Growth_rate'] = m.reactions.BIOMASS_Ec_iML1515_core_75p37M.flux
    # print(m.summary())
    # flux2file(m,'glx',wt)
# %% 
# For GLYAUX
with model as m:
    KORxn(m, df.loc[1,'knock_outs'])
    m.reactions.EX_glyc_e.lower_bound = -1000
    m.reactions.EX_for_e.lower_bound = -1000
    m.reactions.EX_glx.lower_bound = -1
    pfba(m)
    i=len(df)
    df.loc[i]=['GLYAUX',df.loc[1,'knock_outs'],('glyc','for','glx'),'NaN']
    df.loc[i,'Growth_rate'] = m.reactions.BIOMASS_Ec_iML1515_core_75p37M.flux
    # print(m.summary())
    # flux2file(m,'glx',6)

# %%
df['GBR'] = 1 / df['Growth_rate']  # mmol/gCDW = 1 mmol/gCDW/h / 1/h
df['Norm_GBR'] = df['GBR']/df.loc[5,'GBR']
df 
