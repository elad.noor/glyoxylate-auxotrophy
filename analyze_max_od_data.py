import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

CDW_PER_OD = 0.56 # OD660 g / liter, https://doi.org/10.1007/s11306-008-0114-6

def plot_experimental_data(MAX_OD_FNAME: str = "max_od_data.csv") -> plt.Figure:

    # defining colors for all strains
    colors = [
        'xkcd:light orange','tab:red','xkcd:yellow','g','tab:purple','xkcd:neon blue','xkcd:grey',
        'xkcd:light orange','tab:red','xkcd:yellow','g','tab:purple','xkcd:neon blue','xkcd:grey'
    ]
    # defining markers for all strains
    markers = ['s','^','^','o','D','p','8','s','^','^','o','D','p','8']
    max_od_df = pd.read_csv(MAX_OD_FNAME)
    od_lb = 0.1 # OD threshold
    od_ub = 1 # OD threshold
    n = 0
    b = 0
    fig, axs = plt.subplots(2, 1, figsize=(12, 10), dpi=90, sharex=True, sharey=True)
    # adding major and minor gridlines
    axs[0].grid(which='major', axis="both", color='xkcd:grey', linestyle='-',zorder=0)
    axs[0].grid(which='minor', axis="both", color='xkcd:light grey', linestyle=':',zorder=0)
    axs[1].grid(which='major', axis="both", color='xkcd:grey', linestyle='-',zorder=0)
    axs[1].grid(which='minor', axis="both", color='xkcd:light grey', linestyle=':',zorder=0)
    axs[0].set_axisbelow(True)
    axs[1].set_axisbelow(True)

    for i, (carbon_source, cs_df) in enumerate(max_od_df.groupby("carbon_source")):
        ax = axs[i]
        for strain, strain_cs_df in cs_df.groupby("strain"):
            # to go through color arrays defined above
            b = n
            n = b+1
            ax.scatter(x=strain_cs_df.carbon_source_mM, y=strain_cs_df.max_od, c=colors[b], s=1, marker='.', label=None,zorder=1)
            meanods = strain_cs_df.groupby("carbon_source_mM")[["max_od"]].mean()
            stdevs = strain_cs_df.groupby("carbon_source_mM")[["max_od"]].std()

            # only plotting values between defined borders
            mean_ods_sorted = meanods[(od_lb < meanods.max_od) & (meanods.max_od < od_ub)]

            # creating the list to add the respective standard deviations to
            standarddeviations = []
            # defining which concentrations I want to plot standard deviations for
            concentrations = mean_ods_sorted.index.values

            for i in stdevs.index.values:
                if i in concentrations:
                    ind=np.where(stdevs.index.values == i)
                    standarddeviations.append(float(stdevs.max_od.values[ind]))

            offset = np.mean(np.log(mean_ods_sorted.index.values)) - np.mean(np.log(mean_ods_sorted.max_od.values))
            ax.plot([od_lb * np.exp(offset), od_ub * np.exp(offset)], [od_lb, od_ub], c=colors[b], alpha=0.3)

            # the `offset` is in units of mM/OD. To convert to mmol/gCDW we need to divide by CDW_PER_OD
            gbr = np.exp(offset)/CDW_PER_OD
            ax.errorbar(
                x=mean_ods_sorted.index, y=mean_ods_sorted.max_od, yerr=standarddeviations, linewidth=0, elinewidth=1,
                c=colors[b], marker=markers[b], mec="k", ms=7, mew=0.5,
                label=strain + f" (GBR = {gbr:.2f} mmol/gCDW)"
            )
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_ylim(od_lb/10, od_ub*10)
        ax.set_xlabel(carbon_source+" concentration [mM]")
        ax.set_ylabel("Maximal OD")
        ax.set_title(carbon_source)
    return fig