from find_glyoxylate_auxotrophs import calculate_slopes
import warnings
warnings.filterwarnings("ignore", category=UserWarning)

max_num_ko = 3
res_df = calculate_slopes(max_num_ko=max_num_ko, num_processes=16)
res_df = res_df[res_df.slope > 0].sort_values(by="slope", ascending=False)
res_df.to_csv(f"res/auxotrophic_knockouts_max_{max_num_ko}_KOs.csv", index=False)
